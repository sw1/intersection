package body Road_Package is

   protected body Road is
      procedure InsertCar(carPtr: in Car_Access; inserted: out Boolean) is 
      begin
         if blocks(carPtr.position.Y, carPtr.position.X) /= null then
            inserted:= false;
         else
            inserted:= true;
            blocks(carPtr.position.Y, carPtr.position.X):= carPtr;
         end if;
         
      end;
      
      procedure MoveCar(positionFrom, positionTo, positionToCheck: Position_Tuple; moved: out Boolean) is
         carPtr: Car_Access;
      begin
         if (abs positionTo.Y = 13 or abs positionTo.X = 13) then
            -- delete the car from the road
            blocks(positionFrom.Y, positionFrom.X):= null; 
            moved:= true;                    
         elsif blocks(positionTo.Y, positionTo.X) /= null or
            blocks(positionToCheck.Y, positionToCheck.X) /= null then
            moved:= false;
         else
            carPtr:= blocks(positionFrom.Y, positionFrom.X);
            blocks(positionFrom.Y, positionFrom.X):= null;
            blocks(positionTo.Y, positionTo.X):= carPtr;
            moved:= true;            
         end if;         
      end;
      
      function Image return String is
         road_car_image:String:=road_image;
         pos:Integer:=1;
         y:Integer:=11;
         x:Integer:=-11;
      begin
         while y>=-11 loop
            while x<=11 loop
               if blocks(y,x) /= null then
                  road_car_image(pos):=Car_Package.Image(blocks(y,x));
               end if;
               x:=x+2;
               pos:=pos+1;
            end loop;
            pos:=pos+1;
            y:=y-2;
            x:=-11;
         end loop;

         return road_car_image;
      end Image;  
      
      
   end Road;      

end Road_Package;
