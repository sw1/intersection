with Car_Package; use Car_Package;

package Driver_Package is

   stopped:Boolean:=False;
   quit:Boolean:=False;
   pragma Atomic(stopped);
   pragma Atomic(quit);
   subtype Delay_Type is Duration range 0.0..5.0;
   moveDelay: Delay_Type:=2.0;
   pragma Atomic(moveDelay);
   task type Driver is 
      entry Start(carPointer : Car_Access);
      entry Set_Buffer_Id(Id:Integer);
      entry Finish;
   end Driver;
   
   type Driver_Access is access Driver;
 
end Driver_Package;
