with Ada.Text_IO;
with DriverGenerator, Driver_Package, Road_Package;
with Ada.Integer_Text_IO;use Ada.Text_IO;
with traffic_lights_package;
with Driver_Buffer;
use Driver_Buffer;
package body Simulation is

   -- TODO 
   -- procedure showing menu (simulate/quit)
   -- can we exit the program from here??
   procedure showMenu is
   begin
      null;
   end showMenu;
  
   protected body Screen is
      procedure draw is
      begin
         red_vert:=not traffic_lights_package.lights.Check((1,0));
         Ada.Text_IO.Put(ASCII.ESC & "[2J");
         Ada.Text_IO.Put(ASCII.ESC &"[0;0H");
         Put_Line("Symulacja skrzyzowania ze swiatlami");
         Ada.Text_IO.Put(Road_Package.ROAD_OBJECT.Image);
         if Driver_Package.stopped then
            Put_Line("Symulacja zatrzymana");
         else
            Put_Line("Symulacja dziala");
         end if;
         Put_Line("Szybkosc: " & Driver_Package.moveDelay'Img);
         Put_Line("Czerwone pionowo:" & red_vert'Img &" Czas swiatel: " & traffic_lights_package.light_time'Img);
         Put_Line("Sterowanie:");
         Put_Line("q wyjscie; + przyspieszenie; - spowolnienie; s zatrzymanie");
         if(Driver_Package.stopped) then
            Put_line("Ilosc samochod, ktore przejechaly skrzyzowanie: " & Car_Count'Img);
            dur:=Clock - time;
            Put_line("Dlugosc symulacji: " & dur'Img & "s");
         end if;
      end;
   end Screen;
   
   task body Driver_Creator is
      size:Integer;
   begin
      accept Start(size_outer :Integer) do
         size:=size_outer;
      end Start;
      driver_buf:=new Buf(size);
      loop
         driver_buf.Insert;
      end loop;
     exception
      when others=>
         Put_Line("Blad w tworzeniu aut");
   end Driver_Creator;
   
   -- procedure running the simulation
   procedure run is
      carCount:Integer; 
      key:Character;
      driv_creat:Driver_Creator;
   begin
      time:=Clock;
      -- ask user for parameters
      Ada.Text_IO.Put_Line("Podaj liczbe samochodow do wygenerowania");
      Ada.Integer_Text_IO.Get(carCount);
      driv_creat.Start(carCount);
         loop
            scr.draw;
            Get_Immediate(key);
            if key='-' then
               if Driver_Package.moveDelay<=4.9 then
                  Driver_Package.moveDelay:=Driver_Package.moveDelay+0.1;
                  traffic_lights_package.light_time:=traffic_lights_package.light_time+2.0;
               end if;
            elsif key='+' then
               if Driver_Package.moveDelay>=0.1 then
                  Driver_Package.moveDelay:=Driver_Package.moveDelay-0.1;
                  traffic_lights_package.light_time:=traffic_lights_package.light_time-2.0;
               end if;
            elsif key='s' then
               Driver_Package.stopped:=not Driver_Package.stopped;
            elsif key='q' then
               Driver_Package.quit:=True;
               exit;
            end if;
         end loop;
      abort driv_creat;
      driver_buf.Quit;
      dur:=Clock - time;
      abort traffic_lights_package.switcher;
      -- wait till all the cars get out of the road
      Put_line("Dlugosc symulacji: " & dur'Img & "s");
      Put_line("Ilosc samochod, ktore przejechaly skrzyzowanie: " & Car_Count'Img);
      exception
         when DriverGenerator.generator_exception =>
         Ada.Text_IO.Put_Line ("Blad generatora driver!");
      when others =>
         Ada.Text_IO.Put_Line ("Blad zadan");
         return;
   end run;   

end Simulation;
