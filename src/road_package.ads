with Car_Package; use Car_Package;

package Road_Package is


   -- as a type,
   -- because we use it here and this pattern for the dispay
   type CarArray is array (Integer range -11..11, Integer range -11..11) of Car_Access;   
  
   -- Protected type representing the road
   protected type Road is
      procedure InsertCar(carPtr: in Car_Access; inserted: out Boolean);
      procedure MoveCar(positionFrom, positionTo, positionToCheck: Position_Tuple; moved: out Boolean);
      --procedure MoveCar
      
      function Image return String;
   private
      blocks : CarArray;  
   end Road;

   type Road_Access is access Road;
   road_image: constant String:=
     "   |    |   "& Standard.ASCII.LF &
     "   |    |   "& Standard.ASCII.LF &
     "   |    |   "& Standard.ASCII.LF &
     "---*    *---"& Standard.ASCII.LF &
     "            "& Standard.ASCII.LF &
     "            "& Standard.ASCII.LF &
     "            "& Standard.ASCII.LF &
     "            "& Standard.ASCII.LF &
     "---.    .---"& Standard.ASCII.LF &
     "   |    |   "& Standard.ASCII.LF &
     "   |    |   "& Standard.ASCII.LF &
     "   |    |   "& Standard.ASCII.LF ;
   ROAD_OBJECT: Road;   
end Road_Package;


