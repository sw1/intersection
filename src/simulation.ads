with Ada.Calendar;
use Ada.Calendar;
package Simulation is

   procedure showMenu;
   procedure run;
   --procedure showStatistics; --? 

   protected type Screen is
      procedure draw;
      private
      red_vert:Boolean:=True;
   end Screen;
   scr:Screen;
   time:Ada.Calendar.Time;
   dur:Duration;
   task type Driver_Creator is
      entry Start(size_outer:Integer);
   end Driver_Creator;
end Simulation;
