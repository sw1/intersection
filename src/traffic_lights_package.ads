with Car_Package; use Car_Package;

package traffic_lights_package is

   protected type Traffic_Lights is
      procedure SetLights(red_vertical,red_horizontal: Boolean);
      function Check(vec: in Motion_Vector) return Boolean;
   private
      red_vert, red_hori: Boolean:=True;
   end traffic_lights;

   type Traffic_Lights_Access is access Traffic_Lights;

   task type Lights_Switcher(lights: access Traffic_Lights) is
      entry Finish;
   end lights_switcher;

   lights: Traffic_Lights_Access:= new Traffic_Lights;
   switcher: Lights_Switcher(lights);
   light_time:Duration:=55.0;
   pragma Atomic(light_time);
end traffic_lights_package;
