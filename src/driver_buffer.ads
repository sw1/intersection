with Driver_Package;
use Driver_Package;
package Driver_Buffer is
   type TBuf is array(Integer range<>) of Driver_Access;
   protected type Buf (size:Integer) is
      entry Insert;
      entry Delete(Id:in Integer);
      entry Quit;
   private
      count:Integer:=0;
      ptrGet:Integer:=1;
      ptrInsert:Integer:=1;
      buffer:TBuf(1..size);
   end Buf;
   type Buf_access is access Buf;
   driver_buf:Buf_access;

   Car_Count:Integer:=0;
end Driver_Buffer;
