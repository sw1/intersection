with Driver_Package; use Driver_Package;
with Car_Package; use Car_Package;
with Ada.Numerics.Discrete_Random;

package DriverGenerator is

   function GenerateDriver return Driver_Access;

   type Position_Array is array (Integer range <>) of Position_Tuple;
   type Direction_Array is array (Integer range 1..2) of Direction_Vector;
   
   init_positions: constant Position_Array:=
      ((Y=>11, X=>-3),(Y=>11, X=>-1),(Y=>-1, X=>-11),(Y=>-3, X=>-11),
      (Y=>-11, X=>1),(Y=>-11, X=>3),(Y=>1, X=>11),(Y=>3, X=>11));
   subtype positions_range is Integer range init_Positions'Range;
   package random_position is new Ada.Numerics.Discrete_Random (positions_range);
   use random_position;
   seed_pos : random_position.Generator; 
         
   directions_S_R: constant Direction_Array:=
      (DIRECTION_STRAIGHT, DIRECTION_RIGHT);
   directions_S_L: constant Direction_Array:=
      (DIRECTION_STRAIGHT, DIRECTION_LEFT);
   d_S_R,d_S_L: Direction_Vector;
   subtype directions_range is Integer range direction_Array'Range;
   package random_direction is new Ada.Numerics.Discrete_Random (directions_range);
   use random_direction;  
   seed_dir : random_direction.Generator;  
   generator_exception :exception;

end DriverGenerator;
