package Car_Package is

   -- structure representing a point on the road
   -- 13, so we can check if a car is out of the map
   subtype Position_Data is Integer range -13..13;
   type Position_Tuple is record
      Y : Position_Data ;
      X : Position_Data;
   end record;
   
   -- structure representing a vector (current motion and desired change)
   subtype Vector_Data is Integer range -1..1;
   type Vector is record
      Y : Vector_Data ;
      X : Vector_Data;
   end record;
   subtype Motion_Vector is Vector;
   subtype Direction_Vector is Vector;
   
   MOTION_UP: constant Motion_Vector := (Y=> 1, X=> 0);
   MOTION_DOWN: constant Motion_Vector := (Y=> -1, X=> 0);
   MOTION_RIGHT: constant Motion_Vector := (Y=> 0, X=> 1);
   MOTION_LEFT: constant Motion_Vector := (Y=> 0, X=> -1);
   
   DIRECTION_STRAIGHT: constant Direction_Vector := (Y=> 1, X=> 0);
   DIRECTION_RIGHT: constant Direction_Vector := (Y=> 0, X=> 1);
   DIRECTION_LEFT: constant Direction_Vector := (Y=> 0, X=> -1);

 
   -- vehicle object
   type Car is 
      record
         -- current position on the road
         position: Position_Tuple;
         motionVec: Motion_Vector;
         directionVec: Direction_Vector;
      end record;

   type Car_Access is access Car;
   subtype Car_NotNullAccess is not null Car_Access;

   -- return graphical representation of a car
   function Image(car: in Car_NotNullAccess) return Character;
   
end Car_Package;
