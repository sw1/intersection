with Ada.Text_IO;
use Ada.Text_IO;

package body traffic_lights_package is

   task body Lights_Switcher is
      red_vert,red_hori:Boolean;
   begin
      red_vert:=False;
      red_hori:=True;
      lights.SetLights(red_vert, red_hori);
      loop
         select
         accept Finish  do
            null;
         end Finish;
         or
            delay light_time;
            if red_hori then
               red_vert:=True;
               lights.SetLights(red_vert, red_hori);
               delay light_time/10.0;
               red_hori:=False;
               lights.SetLights(red_vert, red_hori);
            else
               red_hori:=True;
               lights.SetLights(red_vert, red_hori);
               delay light_time/10.0;
               red_vert:=False;
               lights.SetLights(red_vert, red_hori);
            end if;
         end select;
      end loop;
   exception
      when others=>
         Put_Line("Blad zmiany swiatel");
   end lights_switcher;

   protected  body traffic_lights is


      procedure SetLights(red_vertical,red_horizontal:Boolean) is
      begin
         red_hori:=red_horizontal;
         red_vert:=red_vertical;
      end;

      function Check (vec : in Motion_Vector) return Boolean is
      begin
         if vec=MOTION_DOWN or vec=MOTION_UP then
            return red_vert;
         elsif vec=MOTION_LEFT or vec=MOTION_RIGHT then
            return red_hori;
         end if;
         return false;
      end Check;


   end traffic_lights;



end traffic_lights_package;
