with Road_Package; use Road_Package;
with Observer_Package;
with traffic_lights_package;
use traffic_lights_package;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Exceptions;
with Driver_Buffer;
package body Driver_Package is

   task body Driver is
   -- declarations
      carPtr: Car_Access;
      nextPosition: Position_Tuple;
      inserted: Boolean;
      moved: Boolean;
      beforeTurning: Boolean:= true;
      Id_Buffer:Integer;
   
      function getNewPosition(pos: Position_Tuple; motionVec: Motion_Vector) return Position_Tuple is
         newPosition: Position_Tuple;
      begin
         newPosition.Y := pos.Y + (2*motionVec.Y);
         newPosition.X := pos.X + (2*motionVec.X);
         return newPosition;
      end; 
      
      function getDestinationPosition return Position_Tuple is
      begin
         return getNewPosition(carPtr.position, carPtr.motionVec);
      end;       
      
      function getNewMotionVec(motionVec: in Motion_Vector; directionVec: in Direction_Vector) return Motion_Vector is   
         new_motionVec: Motion_Vector;   
      begin
         if directionVec=DIRECTION_STRAIGHT then
            new_motionVec := motionVec;
         elsif directionVec=DIRECTION_RIGHT then
            if motionVec=MOTION_UP then new_motionVec:= MOTION_RIGHT;
            elsif motionVec=MOTION_DOWN then new_motionVec:= MOTION_LEFT;
            elsif motionVec=MOTION_RIGHT then new_motionVec:= MOTION_DOWN;
            elsif motionVec=MOTION_LEFT then new_motionVec:= MOTION_UP;
            end if;
            --new_motionVec.Y := motionVec.Y - directionVec.X;
            --new_motionVec.X := directionVec.Y - motionVec.Y;  
         elsif directionVec=DIRECTION_LEFT then
            if motionVec=MOTION_UP then new_motionVec:= MOTION_LEFT;
            elsif motionVec=MOTION_DOWN then new_motionVec:= MOTION_RIGHT;
            elsif motionVec=MOTION_RIGHT then new_motionVec:= MOTION_UP;
            elsif motionVec=MOTION_LEFT then new_motionVec:= MOTION_DOWN;
            end if;
            --new_motionVec.Y := directionVec.X - motionVec.Y;
            --new_motionVec.X := motionVec.Y - directionVec.Y;    
         end if;
         return new_motionVec;
      end;      
      
      procedure turn is
      begin
         if 
         ((abs carPtr.position.Y + abs carPtr.position.X = 8) and 
            (abs nextPosition.Y = 3 and abs nextPosition.X = 3))  or 
         ((abs carPtr.position.Y = 1 and abs carPtr.position.X = 1) and
            (abs nextPosition.Y = 1 and abs nextPosition.X = 1))        
         then
            carPtr.motionVec := getNewMotionVec(carPtr.motionVec, carPtr.directionVec);
            beforeTurning:= false;
         end if;
      end;
      
           
      function getPositionToCheck(destPosition: Position_Tuple) return Position_Tuple is
         positionToCheck: Position_Tuple := destPosition;
      begin
         if ((abs carPtr.position.Y + abs carPtr.position.X = 4) and
               (abs destPosition.Y = 1 and abs destPosition.X = 1)) then
            positionToCheck := getNewPosition(destPosition, carPtr.motionVec);  
         -- CODE BELOW MAY CAUSE STARVATION :(   
         -- else if ((abs carPtr.position.Y = abs carPtr.position.X = 1) and
         --      (abs destPosition.Y = abs destPosition.X = 1)) then
         --   positionToCheck = getNewPosition(destPosition, getNewMotionVec(carPtr.motion, DIRECTION_RIGHT));
         end if;
         
         return positionToCheck;        
         
      end;   
      
      procedure checkLights is
         green: boolean;
      begin
         if (abs carPtr.position.Y = 5 or abs carPtr.position.X = 5) and beforeTurning then  
            loop
               green:=  lights.Check (carPtr.motionVec);
               exit when green;
               delay 0.5;
            end loop;
         end if;
      end;
   
   
   
   
   
   begin
      accept Start(carPointer : Car_Access) do
         carPtr:= carPointer;
      end Start;   
      accept Set_Buffer_Id (Id : in Integer) do
         Id_Buffer:=Id;
      end Set_Buffer_Id;

      loop
         ROAD_OBJECT.InsertCar(carPtr, inserted);
         exit when inserted or quit;
         delay 1.0;
      end loop;
      Observer_Package.Notify;
      if inserted then
         delay moveDelay;
         loop
            if stopped=false then
               checkLights;
               nextPosition := getDestinationPosition;
               if beforeTurning then 
                  turn; 
               end if;
               loop
                  ROAD_OBJECT.MoveCar(carPtr.position, nextPosition, getPositionToCheck(nextPosition), moved);
                  exit when moved;
                  delay 1.0;
               end loop;
               Observer_Package.Notify;
               delay moveDelay;
               carPtr.position:= nextPosition;
               --turn;
               exit when (abs carPtr.position.Y = 13 or abs carPtr.position.X = 13);
            end if;
            delay 1.0;
         end loop;
      end if;
      Observer_Package.Notify;
      Driver_Buffer.driver_buf.Delete(Id_Buffer);
            

   exception
      when exc:others=>
         begin
         Put_Line("Blad samochod:" & carPtr.position.Y'Img & " " &   carPtr.position.X'Img & " " & Ada.Exceptions.Exception_Message(exc)); 
         Put_Line("Position to "&nextPosition.Y'Img & " "& nextPosition.X'Img);
         end;
         
   end Driver;
     
end Driver_Package;









