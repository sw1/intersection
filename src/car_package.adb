package body Car_Package is
 
   -- TODO change the icons - return nice ASCII arrows
   function Image(car: in Car_NotNullAccess) return Character is
   begin
      if car.motionVec=MOTION_UP then
         return '^';
      elsif car.motionVec=MOTION_DOWN then
         return '\';
      elsif car.motionVec=MOTION_RIGHT then
         return '>';
      elsif car.motionVec=MOTION_LEFT then
         return '<';
      end if;
      return 'O';
   end Image;
   
end Car_Package;
