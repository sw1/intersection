with Ada.Numerics.Discrete_Random;
with Ada.Text_IO; use Ada.Text_IO;

package body DriverGenerator is

   function GenerateDriver return Driver_Access is
      driver_ptr: Driver_Access;
      car_ptr: Car_Access;
      
      function GenerateCarInitPosition return Position_Tuple is   
      begin
         return init_positions(Random(seed_pos));
      end;
   
      function GenerateCarInitMotionVector(p: in Position_Tuple) return Motion_Vector is
      begin
         if p=(Y=>-11,X=>1) or p=(Y=>-11, X=>3) then
            return MOTION_UP;
         elsif p=(Y=>11,X=>-3) or p=(Y=>11,X=>-1) then
            return MOTION_DOWN;
         elsif p=(Y=>3,X=>11) or p=(Y=>1,X=>11) then
            return MOTION_LEFT;
         elsif p=(Y=>-1,X=>-11) or p=(Y=>-3,X=>-11) then
            return MOTION_RIGHT;
         end if;
         raise generator_exception;
         return (Y=>0, X=>0);
      end;
   
      function GenerateCarDirectionVector(p: in Position_Tuple) return Direction_Vector is
      begin
         
         d_S_R:=directions_S_R(Random(seed_dir));
         d_S_L:=directions_S_L(Random(seed_dir));
         
         if p=(Y=>11, X=>-3) or p=(Y=>-3, X=>-11) or p=(Y=>-11,X=>3) or p=(Y=>3,X=>11) then
            return d_S_R;
         elsif p=(Y=>11,X=>-1) or p=(Y=>-1, X=>-11) or p=(Y=>-11,X=>1) or p=(Y=>1,X=>11) then
            return d_S_L;
         end if;
         return DIRECTION_STRAIGHT;
      end;
      
   begin
      driver_ptr:= new Driver;
      car_ptr:=new Car;
      car_ptr.position:= GenerateCarInitPosition;
      car_ptr.motionVec:= GenerateCarInitMotionVector(car_ptr.position);
      car_ptr.directionVec:= GenerateCarDirectionVector(car_ptr.position);
      driver_ptr.Start(car_ptr);
      return driver_ptr;
   end;

begin
   random_direction.Reset(seed_dir);
   random_position.Reset(seed_pos);
end DriverGenerator;
