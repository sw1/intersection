with DriverGenerator;
package body Driver_Buffer is
   protected body Buf is
      entry Insert when count<size is
         driver_ptr:Driver_Access;
      begin
         for I in buffer'Range loop
            if buffer(I)=null then
               driver_ptr:=DriverGenerator.GenerateDriver;
               buffer(I):=driver_ptr;
               driver_ptr.Set_Buffer_Id(I);
               count:=count+1;
               exit;
            end if;
         end loop;
      end Insert;

      entry Delete(Id:in Integer) when count>0 is
      begin
         buffer(Id):=null;
         count:=count-1;
         Car_Count:=Car_Count+1;
      end Delete;

      entry Quit when count=0 is
      begin
         null;
      end;
   end Buf;
end Driver_Buffer;
