with Simulation;
with Ada.Text_IO;

package body Observer_Package is

      procedure Notify is
      begin
         pragma Debug (Ada.Text_IO.Put_Line ("Observer.Notify!"));
         Simulation.scr.draw;
      end;

end Observer_Package;
